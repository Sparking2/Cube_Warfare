﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AmmoManager : MonoBehaviour {

    public int ammo;
    public int charger;

    Text txt;


	// Use this for initialization
	void Start () {
        txt = GetComponent<Text>();
        
    }
	
	// Update is called once per frame
	void Update () {
        charger = GameManager.cargador;
        ammo = GameManager.ammo;
        txt.text = charger + "/" + ammo;
	}
}
