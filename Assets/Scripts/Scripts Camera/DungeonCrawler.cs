﻿using UnityEngine;
using System.Collections;

public class DungeonCrawler : MonoBehaviour {

    //Que Transform seguira la camara
	public Transform target;

	public float smoothing = 5f;

    Vector3 offset;
    
	// Buscamos el objeto que seguiremos
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
        offset = transform.position - target.transform.position;
	}

	void FixedUpdate()
	{
		//Pos nos movemos:D
		Vector3 targetCamPos = target.position + offset;
		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);

	}

}
