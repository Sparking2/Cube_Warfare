﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

    public int dañoPorBala = 60;                    // Daño por disparo
    public float tiempoInterDisparo = 1f;         //Tiempo entre cada disparo
    public float rango = 100f;                       //Distancia maxima de la bala

    float timer;                                    // Timer de cuando disparar
    Ray rayoDisparo;                                //Ray de el cañon a donde llega

    RaycastHit impactado;                            // Raycast para saber que golpeamos
    int mascaraImpacto;                              // Mascara para que el raycast solo impacte en la capa
    ParticleSystem disparoParticulas;                // Sistema de Particulas.
    LineRenderer gunLine;                           // Linea a Renderizar.
   
    Light gunLight;                                 // Componente de Iluminacion.
    float effectsDisplayTime = 0.2f;                // Proporcion entre el retraso entre efectos.
    AudioSource GunFX;

    void Awake()
    {
        // Create a layer mask for the Shootable layer.
        mascaraImpacto= LayerMask.GetMask("Impactable");
        gunLine = GetComponent<LineRenderer>();

        // Set up the references.
        disparoParticulas = GetComponentInChildren<ParticleSystem>();

        //gunAudio = GetComponent<AudioSource>();
        gunLight = this.GetComponentInChildren<Light>();

        GunFX = GetComponent<AudioSource>();

    }
    private bool m_isAxisInUse = false;
	// Update is called once per frame
	void Update () {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

		if (Input.GetAxis("Fire" ) > 0 && timer >= tiempoInterDisparo && GameManager.cargador != 0){
			Shoot ();
            GameManager.cargador--;
		}
			
        // If the timer has exceeded the proportion of timeBetweenBullets that the effects should be displayed for...
        if (timer >= tiempoInterDisparo * effectsDisplayTime)
        {
            // ... disable the effects.
            DisableEffects();
        }
        
        if(Input.GetAxisRaw("Reload") != 0 && GameManager.ammo != 0)
        {
            GameManager.cargador = 8;
            if (m_isAxisInUse == false)
            {
                GameManager.ammo--;
                m_isAxisInUse = true;

            }
        }
        if (Input.GetAxisRaw("Reload") == 0)
            m_isAxisInUse = false;
    }

    public void DisableEffects()
    {
        // Disable the line renderer and the light.
        gunLine.enabled = false;
        gunLight.enabled = false;
        disparoParticulas.Stop();
    }

    public void Shoot()
    {
        // Reset the timer.
        timer = 0f;

        // Enable the line renderer and set it's first position to be the end of the gun.
        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);

        // Activar la luz.
        gunLight.enabled = true;

        // Play the gun shot audioclip.
        GunFX.Play();

        // Inicia y detiene las particulas :D.
        disparoParticulas.Stop();
        disparoParticulas.Play();


        rayoDisparo.origin = transform.position;
        rayoDisparo.direction = transform.forward;
        
		if (Physics.Raycast (rayoDisparo, out impactado, rango)) {
			EnemyHealth enemyHealth = impactado.collider.GetComponent<EnemyHealth> ();

			if (enemyHealth != null) {
				enemyHealth.TakeDamage (dañoPorBala, impactado.point);
				//Debug.Log (dañoPorBala);
			}
			gunLine.SetPosition (1, impactado.point);
		} else {
			gunLine.SetPosition (1, rayoDisparo.origin + rayoDisparo.direction * rango);
		}		//Soy 3
     }
   }

