﻿using UnityEngine;

using System.Collections;

public class Bala : MonoBehaviour{

	public Rigidbody rb_bala;
	public float impact_force = 100;
	public Quaternion gun;

	// Use this for initialization
	void Start () {
		rb_bala = GetComponent<Rigidbody>();
        gun = GameObject.FindGameObjectWithTag("Player").transform.rotation;
		rb_bala.AddForce (new Vector3(0,gun.y,0) * impact_force);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnCollisionEnter()
	{
		Invoke ("Matame", 0.5f);
	}

	public void Matame()
	{
		Destroy (gameObject);
	}

}
