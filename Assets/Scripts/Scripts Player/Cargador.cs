﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cargador : MonoBehaviour {

	public static Text Balas;

	// Use this for initialization
	void Start () {
		Balas = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		Balas.text = GameManager.cargador.ToString();
	}
}
