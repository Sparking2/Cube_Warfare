﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public virtualJoystick leftstick;
    public virtualJoystick rightstick;

    public float velocidadMovimiento;
    public float velocidadGiro = 100;

	public GameObject Bala;
	public Transform finBarril;

    Vector3 offset;
    public Rigidbody playerRB;

    public float correction;
    
	// Use this for initialization
	void Start () 
    {
		playerRB = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
    {

        Vector3 dir = new Vector3(rightstick.Horizontal(),rightstick.Vertical());
        if(dir.x != 0 || dir.y != 0)
        {
            float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            angle = angle + correction;
            playerRB.transform.localEulerAngles = new Vector3(0, angle, 0);
        }
        
    }

	void FixedUpdate()
	{
        float moveVertical = leftstick.Vertical();
        float moveHorizontal = leftstick.Horizontal();
       
        //Izquierda
        if (moveHorizontal < 0)
        {
            playerRB.AddForce(moveHorizontal * velocidadMovimiento, 0, 0);
        }

        //Derecha
        if (moveHorizontal > 0)
        {
            playerRB.AddForce(moveHorizontal * velocidadMovimiento, 0, 0);
        }

        //Arriba
        if (moveVertical > 0)
        {
            playerRB.AddForce(0, 0, moveVertical * velocidadMovimiento);
        }

        //Abajo
        if (moveVertical < 0)
        {
            playerRB.AddForce(0, 0, moveVertical * velocidadMovimiento);
        }
        //Velocidad = 0
        playerRB.velocity = new Vector3(0, 0, 0);

        /*float moveVertical = Input.GetAxis("Vertical") * velocidadMovimiento;
		float moveHorizontal = Input.GetAxis ("Horizontal") * velocidadMovimiento;

        Vector3 dir = Vector3.zero;

        dir.x = joystick.Horizontal();
        dir.y = joystick.Vertical();

		if (dir.x > 0) 
		{
			playerRB.AddForce (moveHorizontal,0,0);
		}

		if (Input.GetAxis ("Horizontal") < 0) 
		{
			playerRB.AddForce (moveHorizontal,0,0);
		}
		if (Input.GetAxis ("Vertical") > 0) 
		{
			playerRB.AddForce (0,0,moveVertical);
		}
		if (Input.GetAxis ("Vertical") < 0) 
		{
			playerRB.AddForce (0,0,moveVertical);
		}

		playerRB.velocity = new Vector3 (0, 0, 0);

		if (Input.GetAxis ("Fire") == 1) {
			if (GameManager.cargador > 0) {
				GameObject bala_Clon = (GameObject)Instantiate (Bala, finBarril.position, playerRB.rotation);
				GameManager.cargador -= 1;
			}
		}

		if (Input.GetAxis ("Reload") > 0) {
			GameManager.cargador = 30;
		}*/

    }
}
