﻿using UnityEngine;
using System.Collections;

public class MovimientoPlayer : MonoBehaviour {

	//Movimiento
	public float speed;
	Vector3 movement;
	Rigidbody playerRigidbody;

	//Sticks virtuales
	public lftVirtualJoystick leftstick;
	public virtualJoystick rightstick;

	//Mirar
	public float correction;

	void Awake()
	{
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate()
	{
		float mh = leftstick.Horizontal ();
		float mv = leftstick.Vertical ();
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");

		Move (h, v);
		Move (mh, mv);
		Look ();
	}

	void Move(float h, float v)
	{
		movement.Set (h, 0f, v);
		movement = movement.normalized * speed * Time.deltaTime;
		playerRigidbody.MovePosition (transform.position + movement);
	}

	void Look()
	{
		Vector3 dir = new Vector3(rightstick.Horizontal(),rightstick.Vertical());
		if(dir.x != 0 || dir.y != 0)
		{
			float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
			angle = angle + correction;
			playerRigidbody.transform.localEulerAngles = new Vector3(0, angle, 0);
		}
	}
}
