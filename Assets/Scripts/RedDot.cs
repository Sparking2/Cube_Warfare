﻿using UnityEngine;
using System.Collections;

public class RedDot : MonoBehaviour {
	
	LineRenderer gunLine;
	Ray RayoRedDot;

	// Use this for initialization
	void Start () {
		gunLine = GetComponent<LineRenderer> ();
		gunLine.SetColors (Color.red, Color.red);
	}
	
	// Update is called once per frame
	void Update () {
		RayoRedDot.origin = this.gameObject.GetComponent<Transform>().position;
		RayoRedDot.direction = this.gameObject.GetComponent<Transform> ().forward;
		gunLine.SetPosition (0, this.GetComponent<Transform>().transform.position);
		gunLine.SetPosition (1, RayoRedDot.origin + RayoRedDot.direction * 100);
	}

}
