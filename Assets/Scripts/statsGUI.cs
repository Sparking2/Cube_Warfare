﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class statsGUI : MonoBehaviour {

	public static Text Puntos;
    public static Text Ammo;
	

	// Use this for initialization
	void Start () {
		Puntos = GetComponent<Text> ();
        Ammo = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update () {
		Puntos.text = "Score: " + GameManager.score.ToString ();
        Ammo.text = " " + GameManager.score.ToString();
	}
}
