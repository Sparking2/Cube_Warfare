﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

	public int startingHealth = 100;
	public int currentHealth;
	public float sinkSpeed = 2.5f;
	public int scoreValue = 50;


	//ParticleSystem hitparti;
	bool isDead;
	bool isSinking;

    //Audio Source
    AudioSource enemyAudio;

    //particle System
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;

    // Use this for initialization
    void Awake () {
		currentHealth = startingHealth;
        //enemyAudio = GetComponent<AudioSource>();
        hitParticles = GetComponentInChildren<ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();

	}
	
	// Update is called once per frame
	void Update () {
		if (isSinking) {
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		}
	}

	public void TakeDamage (int amount, Vector3 hitpoint){
		if (isDead)
			return;

        hitParticles.transform.position = hitpoint;
        hitParticles.Play();
        ScoreManager.score += 10;
        currentHealth -= amount;

		if (currentHealth <= 0) {
			Death ();
		}
	}

	void Death(){
		isDead = true;
		StartSinking ();
	}

	public void StartSinking(){
		//Desactivar NavMesh cuando lo use
		GetComponent<NavMeshAgent>().enabled = false;
		GetComponent<Rigidbody>().isKinematic = true;
		isSinking = true;
        ScoreManager.score += scoreValue;
		Destroy (gameObject, 2f);
		//GetComponent<basicEnemy> ().enabled = false;
	}
}
