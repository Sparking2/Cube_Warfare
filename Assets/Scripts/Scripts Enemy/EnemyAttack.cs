﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {

    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    GameObject player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    bool playerInRange;
    float timer;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject == player)
        {
            playerInRange = false;
        }
    }
    
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            playerInRange = true;
    } 

	void Update () {
        timer += Time.deltaTime;
        if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
            Attack();

        if (playerHealth.currentHealth <= 0)
            print("is Dead");
	}

    void Attack()
    {
        timer = 0;
        if (playerHealth.currentHealth > 0)
            playerHealth.TakeDamage(attackDamage);
    }
}
